��          �            x     y  1   �     �     �     �     �  	   �     �  
   �     
     %     =     B  0   W     �  '  �      �  A   �  	      	   *     4     J     O     \     o     }     �     �     �  0   �                                           	             
                             &mdash; No Change &mdash; A simple way to change a post's type in WordPress Cancel Edit Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Triple J Software, Inc. Type https://jjj.software https://wordpress.org/plugins/post-type-switcher verbSponsor PO-Revision-Date: 2021-07-29 08:37:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Post Type Switcher - Development (trunk)
 &mdash; Geen wijzigingen &mdash; Een eenvoudige manier om het berichttype te wijzigen in WordPress Annuleren Bewerkend Ontbrekende gegevens. Oké Bericht Type Post Type Switcher Bericht type: Helaas, je kunt dit niet doen. Triple J Software, Inc. Type: https://jjj.software https://wordpress.org/plugins/post-type-switcher Sponsor 