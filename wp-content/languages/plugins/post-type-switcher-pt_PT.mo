��          �            h     i  M   �     �     �     �     �     �  	         
  
        (     C  /   H  1   x  )  �     �  \   �     L     U     \     n     ~     �     �     �     �     �  /   �  1                                   	       
                                         &mdash; No Change &mdash; Allow switching of a post type while editing a post (in post publish section) Cancel Edit John James Jacoby Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Type https://profiles.wordpress.org/johnjamesjacoby/ https://wordpress.org/plugins/post-type-switcher/ PO-Revision-Date: 2020-04-30 09:26:55+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: pt
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; No alterar &mdash; Permite mudar o tipo de conteúdo na página de edição do conteúdo (na secção Publicar) Cancelar Editar John James Jacoby Dados em falta. OK Tipo de conteúdo Post Type Switcher Tipo de conteúdo: Desculpe, não pode fazer isto. Tipo https://profiles.wordpress.org/johnjamesjacoby/ https://wordpress.org/plugins/post-type-switcher/ 