��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  -   �  *  �           ;  2   \  !   �     �     �     �     �          '     5  A   C  0   �     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ PO-Revision-Date: 2021-10-02 19:36:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nb_NO
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Kunne ikke bytte til utlogget. Kunne ikke bytte mellom brukere. Bytt enkelt mellom ulike brukerkontoer i WordPress John Blackbourn &amp; bidragytere Bytt til utlogget Bytt tilbake til %1$s (%2$s). Bytt&nbsp;til Byttet tilbake til %1$s (%2$s). Byttet til %1$s (%2$s). Brukerbytting Brukerbytting https://github.com/johnbillion/user-switching/graphs/contributors https://nb.wordpress.org/plugins/user-switching/ 