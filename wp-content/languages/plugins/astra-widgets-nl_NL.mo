��    Q      �  m   ,      �     �                ;  8   G     �     �  	   �     �     �  	   �  =   �     �               (     ?     P     g     x     �     �     �     �     �     �     �     �     �     �     	     	     %	     ,	     3	     :	     A	     F	     K	  
   ^	     i	  
   z	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  	   �	  	   �	      
     
  	   
      
     .
     5
     E
     K
     f
     n
     u
     �
     �
     �
  @   �
  7   �
                 �   .     �     �     �     �     �  &       ?     [  !   v     �  5   �  	   �     �     �            
     C   &     j     x     �     �     �     �     �     �                  	        (     4     A     R     j     �     �     �  
   �     �     �     �     �  	   �     �          $     9     H  
   Z     e     z     �  
   �  	   �  
   �  
   �     �     �     �     �     �  	             %     8     D     V  !   ]          �     �     �     �     �  F   �  4        6     <     C  �   U  !   �               +     @     '             B       8   4   L      3             D          6       ?       ,   9      "   >           N   O                (   P       E                   5   &          	   @       %         1              $   <   F           #       C   I   7   ;       A   H       2   0      :         G                        .           /   =   )   K       !          M   Q      +   J   
   -   *                  	Space Between Address Fields: 	Space Between List Items: 	Space Between Social Profiles:  Know More.  This will be applicable for all sites from the network.  Weight: Add Item Add Item: Add Profile Address: Alignment Allow %s products to track non-sensitive usage tracking data. Astra Widgets Astra: Address Astra: List Icons Astra: Social Profiles Background Color Background Hover Color Brainstorm Force Choose Icon.. Circle Circle Outline Color: Custom Dashed Disable Display Address. Display Icons? Display list icons. Display profile title? Display social profiles. Divider Dotted Double Email: Enable FAX: Icon Icon / Image Style Icon Color Icon Hover Color Icon Style Icon Width: Image Image / Icon Image / Icon Size: Inline Learn More. Link Link: List Item: New Page No No Follow No Thanks Official Color Phone: Same Page Show Divider: Simple Social Profiles Solid Space Between Icon & Text: Spacing Square Square Outline Stack Style: Styling The Fastest Way to Add More Widgets into Your WordPress Website. This will be applicable for all sites from the network. Title Title: Usage Tracking Want to help make <strong>%1s</strong> even more awesome? Allow us to collect non-sensitive diagnostic data and usage information.  WordPress Nonce not validated. Yes Yes! Allow it https://wpastra.com/ https://www.brainstormforce.com PO-Revision-Date: 2021-12-08 20:48:05+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Astra Widgets - Stable (latest release)
 	Ruimte tussen adresvelden: 	Ruimte tussen lijstitems: 	Ruimte tussen sociale profielen:  Meer weten.  Dit is van toepassing op alle sites van het netwerk.  Gewicht: Voeg item toe Voeg item toe: Profiel toevoegen Adres: Uitlijning Sta %s producten toe niet-gevoelige gebruiksgegevens bij te houden. Astra Widgets Astra: Adres Astra: Lijstpictogrammen Astra: sociale profielen Achtergrondkleur Achtergrond hoverkleur Brainstorm Force Kies pictogram. Cirkel Cirkeloverzicht Kleur: Aangepast Onderbroken Uitschakelen Adres weergeven. Pictogrammen weergeven? Lijstpictogrammen weergeven. Profieltitel weergeven? Geef sociale profielen weer. Divider Gestippeld Dubbele E-mail: Inschakelen FAX: Pictogram Pictogram/afbeeldingsstijl Pictogramkleur Pictogram hoverkleur Pictogramstijl Pictogrambreedte: Afbeelding Afbeelding/pictogram Grootte afbeelding/pictogram: In lijn Leer meer. Koppeling Koppeling: Lijstitem: Nieuwe pagina Nee Geen Volgen Nee, dank je wel Officiële kleur Telefoon: Dezelfde pagina Divider weergeven: Gemakkelijk Sociale Profielen Solide Ruimte tussen pictogram en tekst: Spatiëring Vierkant Vierkant omtrek Stapel Stijl: Styling De snelste manier om meer widgets aan je WordPress-site toe te voegen. Dit is van toepassing op alle sites van het netwerk. Titel Titel: Gebruik bijhouden Wil je helpen om <strong> %1s </strong> nog indrukwekkender te maken? Sta ons toe om niet gevoelige diagnostische gegevens en gebruiksinformatie te verzamelen.  WordPress Nonce niet gevalideerd. Ja Ja! Sta het toe https://wpastra.com/ https://www.brainstormforce.com 