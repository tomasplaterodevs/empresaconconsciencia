��          |      �             !     7  4   O  
   �     �     �     �     �     �  :   �  ;   :  �  v  %     B   .  v   q     �  /   �     )  +   C      o     �     �  ;   �            	                                           
    Could not switch off. Could not switch users. Instant switching between user accounts in WordPress Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2015-04-10 03:17:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: uk_UA
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Не вдалося вимкнути. Не вдалося перемкнути користувачів. Швидке перемикання між обліковими записами користувачів у WordPress Вимкнути Перемкнути назад на%1$s (%2$s) Перемкнути на Змінено назад на %1$s (%2$s). Змінено на %1$s (%2$s). User Switching User Switching https://johnblackbourn.com/wordpress-plugin-user-switching/ 