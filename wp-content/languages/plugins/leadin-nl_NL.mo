��            )   �      �     �     �     �     �     �  ^   �  7   )     a  �   n     �  L        e  	   k     u  	   {     �     �     �     �     �     �  (   �  l   #  |   �          %     >  %   X  -   ~  [  �  	                  &     2  V   :  7   �     �  �   �  $   |	  ;   �	     �	  	   �	     �	     �	     
      
  +   <
     h
     u
     �
  ,   �
  o   �
  �   C     �     �     �  %     -   <                                                        
              	                                                                   Contacts Email Feedback... Forms HubSpot HubSpot All-In-One Marketing %1$s requires PHP %2$s or higher. Please upgrade WordPress first. HubSpot All-In-One Marketing - Forms, Popups, Live Chat HubSpot Form HubSpot’s official WordPress plugin allows you to add forms, popups, and live chat to your website and integrate with the best WordPress CRM. I can't sign up or log in If you have a moment, please let us know why you're deactivating the plugin. Lists Live Chat Other Reporting Select a form Select an existing form Select and embed a HubSpot form Settings Skip & deactivate Submit & deactivate Temporarily disabling or troubleshooting Thank you for your feedback. If you would like to tell us more please add your email and we'll get in touch. The HubSpot plugin isn’t connected right now. To use HubSpot tools on your WordPress site, %1$sconnect the plugin now%2$s. The plugin isn't useful The plugin isn't working We're sorry to see you go http://hubspot.com/products/wordpress http://www.hubspot.com/integrations/wordpress PO-Revision-Date: 2021-05-31 19:54:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - HubSpot &#8211; CRM, Email Marketing, Live Chat, Forms &amp; Analytics - Development (trunk)
 Contacten E-mail Feedback ... Formulieren HubSpot HubSpot Alles-In-Een Marketing %1$s vereist PHP %2$s of hoger. Update WordPress eerst. HubSpot All-In-One Marketing - Forms, Popups, Live Chat HubSpot Formulier HubSpot's officiële WordPress plugin staat je toe om formulieren, popups, en live chats aan je site toe te voegen en te integreren met het beste WordPress CRM. Ik kan me niet aanmelden of inloggen Laat ons alsjeblieft weten waarom je de plugin deactiveert. Lijsten Live Chat Overige Rapporteren Selecteer een formulier Selecteer een bestaand formulier Selecteer en sluit een Hubspot formulier in Instellingen Overslaan en deactiveren Verzenden & deactiveren Tijdelijk uitschakelen of problemen oplossen Bedankt voor je feedback. Als je ons meer wilt vertellen, voeg dan je e-mail toe en we nemen contact met je op. De HubSpot plugin is momenteel niet verbonden. Om HubSpot gereedschap op je WordPress site te gebruiken, verbind%1$s de plugin nu%2$s. De plugin is niet nuttig De plugin werkt niet Het spijt ons je te zien gaan http://hubspot.com/products/wordpress http://www.hubspot.com/integrations/wordpress 