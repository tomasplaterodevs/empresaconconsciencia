��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  -   �  *  �           =  A   [     �     �      �     �  #        '     B     Q  A   c  -   �     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ PO-Revision-Date: 2021-10-03 13:33:01+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: sv_SE
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Kunde inte växla till utloggad. Kunde inte växla användare. Växla snabbt och enkelt mellan olika användarkonton i WordPress John Blackbourn & contributors Växla till utloggad Växla tillbaka till %1$s (%2$s) Växla&nbsp;till Växlade tillbaka till %1$s (%2$s). Växlade till %1$s (%2$s). User Switching Växla användare https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ 