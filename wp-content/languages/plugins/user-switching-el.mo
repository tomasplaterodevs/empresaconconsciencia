��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  ;   �  *  �  %   *  2   P  g   �     �     
  *        F  0   a  "   �     �     �  A   �  ;   &     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2018-08-03 11:32:42+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: el_GR
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Αδύνατο το κλείσιμο. Αδύνατη η εναλλαγή χρηστών. Στιγμιαία εναλλαγή μεταξύ λογαριασμών χρηστών στο WordPress John Blackbourn & contributors Κλείσιμο Εναλλαγή ξανά σε %1$s (%2$s) Εναλλαγή&nbsp;Σε Εναλλάχθηκε ξανά σε %1$s (%2$s) Εναλλαγή σε %1$s (%2$s). User Switching Εναλλαγή Χρηστών https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ 