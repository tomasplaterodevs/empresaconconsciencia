��          �            h     i  1   �     �     �     �     �     �  	   �     �  
             '     ,  0   =  *  n  #   �  H   �                    )     >     A     U     h  (        �     �  0   �                                      
                	                          &mdash; No Change &mdash; A simple way to change a post's type in WordPress Cancel Edit John James Jacoby Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Type https://jjj.blog https://wordpress.org/plugins/post-type-switcher PO-Revision-Date: 2021-04-04 08:36:50+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; Aucune modification &mdash; Une façon simple de modifier le type d’une publication dans WordPress Annuler Modifier John James Jacoby Données manquantes. OK Type de publication Post Type Switcher Type de publication : Désolé, vous ne pouvez pas faire cela. Type https://jjj.blog https://wordpress.org/plugins/post-type-switcher 