��    Q      �  m   ,      �     �                ;  8   G     �     �  	   �     �     �  	   �  =   �     �               (     ?     P     g     x     �     �     �     �     �     �     �     �     �     �     	     	     %	     ,	     3	     :	     A	     F	     K	  
   ^	     i	  
   z	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  	   �	  	   �	      
     
  	   
      
     .
     5
     E
     K
     f
     n
     u
     �
     �
     �
  @   �
  7   �
                 �   .     �     �     �     �     �  �    ?   �  ;   �  O   "     r  O   �     �     �           0     P     \  �   u     �       "   &  *   I     t  +   �     �     �     �     �  	              0     ?      R  "   s  3   �  7   �  9        <     S     h     w     ~  	   �     �  0   �     �  /   �          7     R  %   i  3   �     �     �     �                1     M  	   T     ^     u     �     �  (   �     �  "   �       E   ,     r     �  !   �     �     �     �  u   �  I   h     �     �  3   �      ,   '     T     Y     r     �     '             B       8   4   L      3             D          6       ?       ,   9      "   >           N   O                (   P       E                   5   &          	   @       %         1              $   <   F           #       C   I   7   ;       A   H       2   0      :         G                        .           /   =   )   K       !          M   Q      +   J   
   -   *                  	Space Between Address Fields: 	Space Between List Items: 	Space Between Social Profiles:  Know More.  This will be applicable for all sites from the network.  Weight: Add Item Add Item: Add Profile Address: Alignment Allow %s products to track non-sensitive usage tracking data. Astra Widgets Astra: Address Astra: List Icons Astra: Social Profiles Background Color Background Hover Color Brainstorm Force Choose Icon.. Circle Circle Outline Color: Custom Dashed Disable Display Address. Display Icons? Display list icons. Display profile title? Display social profiles. Divider Dotted Double Email: Enable FAX: Icon Icon / Image Style Icon Color Icon Hover Color Icon Style Icon Width: Image Image / Icon Image / Icon Size: Inline Learn More. Link Link: List Item: New Page No No Follow No Thanks Official Color Phone: Same Page Show Divider: Simple Social Profiles Solid Space Between Icon & Text: Spacing Square Square Outline Stack Style: Styling The Fastest Way to Add More Widgets into Your WordPress Website. This will be applicable for all sites from the network. Title Title: Usage Tracking Want to help make <strong>%1s</strong> even more awesome? Allow us to collect non-sensitive diagnostic data and usage information.  WordPress Nonce not validated. Yes Yes! Allow it https://wpastra.com/ https://www.brainstormforce.com PO-Revision-Date: 2022-01-15 12:55:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: ru
Project-Id-Version: Plugins - Astra Widgets - Stable (latest release)
 	Пространство между полями адреса: 	Пробел между элементами списка: 	Пространство между социальными профилями:  Узнать больше.  Это будет применимо для всех сайтов в сети.  Толщина: Добавить элемент Добавить элемент: Добавить профиль Адрес: Выравнивание Разрешить продуктам %s отслеживать некритичные данные об их использовании. Виджеты Astra Astra: адрес Astra: список значков Astra: социальные профили ﻿Цвет фона Цвет фона при наведении Brainstorm Force Выбрать значок.. Круг Контур круга Цвет: Пользовательский Пунктир Выключить Отображать адрес. Отображать значки? Отображение список значков. Отображать заголовок профиля? Отображать социальные профили. Разделитель Пунктирный Двойная Email: Разрешить Факс: Значок Значок / Стиль изображения Цвет значка Цвет значка при наведении Стиль значка Ширина значка: Изображение Изображение / значок Изображение / размер значка: В одну линию Узнать больше. Ссылка Ссылка: Список элементов: Новая страница Нет No Follow Нет, спасибо Официальный цвет Телефон: Та же страница Показать разделитель: Простой Профили в соцсетях Сплошной Пространство между значком и текстом: Интервал Квадрат Квадратный контур Друг под другом Стиль: Стилизация Самый быстрый способ добавить больше виджетов на ваш сайт WordPress. Это будет применимо ко всем сайтам сети. Заголовок Заголовок: Отслеживание использования Хотите помочь сделать <strong>%1s</strong> еще более потрясающим? Разрешите нам собирать нечувствительные диагностические данные и информацию об использовании.  WordPress Nonce не подтвержден. Да Да! Разрешить https://wpastra.com/ https://www.brainstormforce.com 