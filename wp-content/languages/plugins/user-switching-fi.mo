��          |      �             !     7  4   O  
   �     �     �     �     �     �  :   �  ;   :  '  v  &   �     �  A   �     '  *   3     ^  -   m  %   �     �     �  ;   �            	                                           
    Could not switch off. Could not switch users. Instant switching between user accounts in WordPress Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2015-10-15 18:34:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fi
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Käyttäjä ei voitu vaihtaa takaisin. Käyttäjää ei voitu vaihtaa. Vaihda käyttäjää ilman erillistä kirjautumista WordPress:iin Vaihda pois Vaihda takaisin käyttäjään %1$s (%2$s) Vaihda tähän Vaihdettu takaisin käyttäjään %1$s (%2$s) Vaihdettu käyttäjään %1$s (%2$s). Vaihda käyttäjää Vaihda käyttäjää https://johnblackbourn.com/wordpress-plugin-user-switching/ 