��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  ;   �  '  �  0   '  ,   X  4   �     �     �     �               6     N     ]  A   k  ;   �     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2018-12-29 19:28:58+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: it
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Non è stato possibile uscire dal cambio utente. Non è stato possibile scambiare gli utenti. Consente il cambio istantaneo di utente in WordPress John Blackbourn e collaboratori Disconnettiti temporaneamente Torna a %1$s (%2$s) Cambia&nbsp;Utente Tornato a %1$s (%2$s). Cambiato a %1$s (%2$s). User Switching Cambio utente https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ 