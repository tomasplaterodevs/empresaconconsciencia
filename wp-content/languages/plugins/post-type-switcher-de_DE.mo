��          �            h     i  1   �     �     �     �     �     �  	   �     �  
             '     ,  0   =  +  n     �  9   �  	   �     �               ,  
   2     =     P  -   \     �     �  4   �                                      
                	                          &mdash; No Change &mdash; A simple way to change a post's type in WordPress Cancel Edit John James Jacoby Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Type https://jjj.blog https://wordpress.org/plugins/post-type-switcher PO-Revision-Date: 2021-05-06 19:37:06+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: de
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; Keine Änderung &mdash; Ein einfacher Weg, den Inhaltstyp in WordPress zu ändern Abbrechen ﻿Bearbeiten John James Jacoby Daten fehlen. ﻿OK Inhaltstyp Post Type Switcher Inhaltstyp: Entschuldigung, aber das kannst du nicht tun. Typ https://jjj.blog https://de.wordpress.org/plugins/post-type-switcher/ 