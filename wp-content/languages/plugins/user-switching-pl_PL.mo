��          |      �             !     7  4   O  
   �     �     �     �     �     �  :   �  ;   :  x  v  %   �  *     I   @     �      �     �  %   �     �     	        ;   7            	                                           
    Could not switch off. Could not switch users. Instant switching between user accounts in WordPress Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2015-10-15 18:35:13+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: pl
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Wyłączenie się nie było możliwe. Zamiana użytkowników nie była możliwa. Natychmiastowe przełączanie pomiędzy kontami użytkowników WordPressa Wyłącz się Zmień z powrotem na %1$s (%2$s) Zmień&nbsp;na Zamieniono z powrotem na %1$s (%2$s). Zamieniono na %1$s (%2$s). Zamiany użytkowników Zamiany użytkowników https://johnblackbourn.com/wordpress-plugin-user-switching/ 