��          �            h     i  1   �     �     �     �     �     �  	   �     �  
             '     ,  0   =  .  n  (   �  ~   �  
   E     P     g      y     �     �     �     �  N   �  
   2     =  0   N                                      
                	                          &mdash; No Change &mdash; A simple way to change a post's type in WordPress Cancel Edit John James Jacoby Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Type https://jjj.blog https://wordpress.org/plugins/post-type-switcher PO-Revision-Date: 2021-01-25 10:13:00+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: el_GR
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; Καμία αλλαγή &mdash;  Ένας απλός τρόπος για να αλλάξετε τον τύπο μιας καταχώρησης στο WordPress Άκυρο Επεξεργασία John James Jacoby Ελλειπή δεδομένα. ΟΚ Τύπος άρθρου Post Type Switcher Τύπος άρθρου: Λυπάμαι, δεν επιτρέπεται να το κάνετε αυτό. Είδος https://jjj.blog https://wordpress.org/plugins/post-type-switcher 