��          �      �       0     1     G  4   _  
   �     �     �     �     �        :     A   J  ;   �  &  �     �  !     /   &     V     ^     }  !   �     �     �     �  A   �  ;   4            	                                          
    Could not switch off. Could not switch users. Instant switching between user accounts in WordPress Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2021-08-10 15:54:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: id
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Tidak dapat beralih. Tidak dapat mengalihkan pengguna. Beralih cepat antara akun pengguna di WordPress Beralih Beralih kembali ke %1$s (%2$s) Beralih&nbsp;Ke Dialihkan kembali ke %1$s (%2$s). Dialihkan ke %1$s (%2$s). Pengalihan Pengguna Mengalihkan Pengguna https://github.com/johnbillion/user-switching/graphs/contributors https://johnblackbourn.com/wordpress-plugin-user-switching/ 