��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  -   �  '  �       '   7  9   _     �     �     �     �  !        #     B     Q  A   `  -   �     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ PO-Revision-Date: 2021-10-03 09:46:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Kon niet tijdelijk uitloggen. Kon niet omschakelen tussen gebruikers. Direct omschakelen tussen gebruikersaccounts in WordPress John Blackbourn & contributors Afmelden simuleren Terugschakelen naar %1$s (%2$s) Naar&nbsp;omschakelen Teruggeschakeld naar %1$s (%2$s). Omgeschakeld naar %1$s (%2$s). User Switching User Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ 