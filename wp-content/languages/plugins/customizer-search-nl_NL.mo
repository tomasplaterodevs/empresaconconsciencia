��          �      L      �     �  8   �  =        D     U     [     m  	   y     �  "   �  	   �  7   �     �  �   �     �     �  /   �      �  *        +  +   8  C   d     �     �     �  
   �     �     �  '   �       4         U  �   g  !   
     ,  /   <      l                     
                                     	                                           Know More.  This will be applicable for all sites from the network. Allow %s products to track non-sensitive usage tracking data. Brainstorm Force Clear Customizer Search Learn More. No Thanks Search Search for settings in customizer. Search... This will be applicable for all sites from the network. Usage Tracking Want to help make <strong>%1s</strong> even more awesome? Allow us to collect non-sensitive diagnostic data and usage information.  WordPress Nonce not validated. Yes! Allow it https://github.com/Nikschavan/customizer-search https://www.brainstormforce.com/ PO-Revision-Date: 2021-11-07 13:02:36+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Customizer Search - Stable (latest release)
  Meer weten.  Dit geldt voor alle sites van het netwerk. Sta %s producten toe niet-gevoelige gebruiksgegevens bij te houden. Brainstorm Force Wissen Customizer Search Leer meer. Nee bedankt Zoeken Zoeken naar instellingen in customizer. Zoek... Dit is van toepassing op alle sites van het netwerk. Gebruik bijhouden Wil je ons helpen om <strong>%1s</strong> nog indrukwekkender te maken? Sta ons toe om niet-gevoelige diagnostische gegevens en gebruiksinformatie te verzamelen.  WordPress Nonce niet gevalideerd. Ja! Sta het toe https://github.com/Nikschavan/customizer-search https://www.brainstormforce.com/ 