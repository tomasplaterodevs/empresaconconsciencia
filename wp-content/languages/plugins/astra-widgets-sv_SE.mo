��    L      |  e   �      p     q     �     �     �     �     �  	   �     �     �  	                   /     F     W     n          �     �     �     �     �     �     �     �     �     �                    &     -     4     9     >  
   Q     \  
   m     x     �     �     �     �     �     �     �  
   �     �     �  	   �  	   �     �     	  	   		     	     !	     (	     8	     >	     Y	     a	     h	     w	     }	     �	  @   �	  7   �	     
     
     
     !
     @
     D
     R
     g
  )  �
     �     �  !   �               #     5     H     Z  	   b     l     z     �     �     �     �     �     �     �                 
        *     7     D     W     n     w          �     �     �     �     �  	   �     �     �     �     �  	   �     �               )     /     6     B     J  
   N     Y     b     r  
   {     �     �     �     �     �  	   �     �     �     �     �       P     ?   c     �     �     �  %   �     �     �                  A   !          :                   5   F      	          B   -      
   )      @   $   "   3       1   ;   .      C       I       =       7         #         0   2          J       E       (      *                         &   <          K          8           ?   G   +             /       6   ,   '          >              H   %          D       L      4      9        	Space Between Address Fields: 	Space Between List Items: 	Space Between Social Profiles:  Know More.  Weight: Add Item Add Item: Add Profile Address: Alignment Astra Widgets Astra: Address Astra: Social Profiles Background Color Background Hover Color Brainstorm Force Choose Icon.. Circle Circle Outline Color: Custom Dashed Disable Display Address. Display Icons? Display profile title? Display social profiles. Divider Dotted Double Email: Enable FAX: Icon Icon / Image Style Icon Color Icon Hover Color Icon Style Icon Width: Image Image / Icon Image / Icon Size: Inline Learn More. Link Link: List Item: New Page No No Follow No Thanks Official Color Phone: Same Page Show Divider: Simple Social Profiles Solid Space Between Icon & Text: Spacing Square Square Outline Stack Style: Styling The Fastest Way to Add More Widgets into Your WordPress Website. This will be applicable for all sites from the network. Title Title: Usage Tracking WordPress Nonce not validated. Yes Yes! Allow it https://wpastra.com/ https://www.brainstormforce.com PO-Revision-Date: 2021-12-26 15:36:33+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: sv_SE
Project-Id-Version: Plugins - Astra Widgets - Stable (latest release)
 	avstånd mellan adressfält: 	Mellanrum mellan listobjekt: 	Utrymme mellan sociala profiler: Lär dig mer. Vikt: Lägg till objekt Lägg till objekt: Lägg till profil Adress: Justering Astra Widgets Astra: Adress Astra: Sociala profiler Bakgrundsfärg Bakgrundens hovringsfärg Brainstorm Force Välj ikon… Cirkel Cirkelformad yttre ram Färg: Anpassad Streckad Inaktivera Visa adress. Visa ikoner? Visa profilrubrik? Visa sociala profiler. Avdelare Prickad Dubbel E-post: Aktivera FAX: Ikon Stil för ikon/bild Ikonfärg Ikonens hovringsfärg Ikonstil Bredd på ikon: Bild Bild/ikon Storlek på bild/ikon: Inline Lär dig mer. Länk Länk: Listobjekt: Ny sida Nej Följ inte Nej tack Officiell färg Telefon: Samma sida Visa avdelare: Enkel Sociala profiler Solid Utrymme mellan ikon och text: Mellanrum Fyrkant Kvadratisk yttre ram Stapel Stil: Stilsättning Det snabbaste sättet att lägga till fler widgetar på din WordPress-webbplats. Detta kommer att gälla för alla webbplatser från nätverket. Rubrik Rubrik: Användningsspårning WordPress engångskod inte validerad. Ja Ja! Tillåt det https://wpastra.com/ https://www.brainstormforce.com 