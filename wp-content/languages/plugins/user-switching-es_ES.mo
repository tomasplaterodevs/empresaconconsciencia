��          �      �       H     I     _  4   w     �  
   �     �     �                7  :   F  A   �  -   �  '  �       #   6  A   Z     �     �     �     �      �          /     >  A   Q  0   �     
                             	                               Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ PO-Revision-Date: 2021-10-03 05:05:08+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: es
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 No se ha podido desconectar. No se ha podido cambiar de usuario. Cambio instantáneo de una cuenta de usuario a otra en WordPress  John Blackbourn y colaboradores Desconectar Cambiar de nuevo a %1$s (%2$s) Cambiar&nbsp;a Cambiado de nuevo a %1$s (%2$s). Cambiado a %1$s (%2$s). User Switching Cambiar de usuario https://github.com/johnbillion/user-switching/graphs/contributors https://es.wordpress.org/plugins/user-switching/ 