DROP TABLE IF EXISTS `b_wp_revisr`;
CREATE TABLE `b_wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  `user` varchar(60) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
LOCK TABLES `b_wp_revisr` WRITE;
INSERT INTO `b_wp_revisr` VALUES ('1','2022-01-16 09:47:39','Successfully created a new repository.','init','freelance@tomasplatero.dev'), ('2','2022-01-16 10:01:05','Committed <a href=\"https://wordpress-708506-2349177.cloudwaysapps.com/wp-admin/admin.php?page=revisr_view_commit&commit=4e27cc0&success=true\">#4e27cc0</a> to the local repository.','commit','freelance@tomasplatero.dev'), ('3','2022-01-16 10:01:49','Error pushing changes to the remote repository.','error','freelance@tomasplatero.dev'), ('4','2022-01-16 10:04:43','Error pushing changes to the remote repository.','error','freelance@tomasplatero.dev');
UNLOCK TABLES;
