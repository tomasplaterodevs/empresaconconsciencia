# Restrict Content Pro - Per-Level Emails

Configure different activation, expiration, and cancellation emails for each membership level.