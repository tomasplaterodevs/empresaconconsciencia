<?php
/**
 * Plugin Name: Restrict Content Pro - Per-Level Emails
 * Plugin URI: https://restrictcontentpro.com/downloads/per-level-emails/
 * Description: Configure different activation, expiration, and cancellation emails for each membership level.
 * Author: Sandhills Development, LLC
 * Author URI: https://sandhillsdev.com
 * Version: 1.0.1
 * Text Domain: rcp-per-level-emails
 * License: GPL2
 * iThemes Package: rcp-per-level-emails
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * Define constants.
 */
define( 'RCP_PER_LEVEL_EMAILS_VERSION', '1.0.1' );
define( 'RCP_PER_LEVEL_EMAILS_PATH', plugin_dir_path( __FILE__ ) );
define( 'RCP_PER_LEVEL_EMAILS_URL', plugin_dir_url( __FILE__ ) );

/**
 * Check for requirements and load the plugin.
 *
 * @since 1.0
 * @return void
 */
function loader() {

	if ( ! defined( 'RCP_PLUGIN_VERSION' ) || version_compare( RCP_PLUGIN_VERSION, '3.0', '<' ) ) {
		add_action( 'admin_notices', __NAMESPACE__ . '\incompatible_version_notice' );

		return;
	}

	require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/filters.php';
	require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/functions.php';

	if ( is_admin() ) {
		require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/admin/admin-notices.php';
		require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/admin/admin-pages.php';
		require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/admin/settings.php';
	}

}

add_action( 'plugins_loaded', __NAMESPACE__ . '\loader' );

/**
 * Displays an admin notice if RCP is not installed or not using the required version.
 *
 * @since 1.0
 * @return void
 */
function incompatible_version_notice() {
	?>
	<div class="error">
		<p><?php _e( 'Restrict Content Pro - Per-Level Emails requires Restrict Content Pro version 3.0 or higher. Please upgrade Restrict Content Pro to the latest version.', 'rcp-per-level-emails' ); ?></p>
	</div>
	<?php
}

/**
 * Loads the plugin translation files.
 *
 * @since 1.0
 * @return void
 */
function textdomain() {
	load_plugin_textdomain( 'rcp-per-level-emails', false, RCP_PER_LEVEL_EMAILS_PATH . 'languages' );
}

add_action( 'plugins_loaded', __NAMESPACE__ . '\textdomain' );

/**
 * Loads the plugin updater.
 *
 * @since 1.0
 * @return void
 */
function plugin_updater() {
	if ( is_admin() && class_exists( 'RCP_Add_On_Updater' ) ) {
		new \RCP_Add_On_Updater( 141150, __FILE__, RCP_PER_LEVEL_EMAILS_VERSION );
	}
}

add_action( 'plugins_loaded', __NAMESPACE__ . '\plugin_updater' );


if ( ! function_exists( 'ithemes_repository_name_updater_register' ) ) {
	function ithemes_repository_name_updater_register( $updater ) {
		$updater->register( 'rcp-per-level-emails', __FILE__ );
	}
	add_action( 'ithemes_updater_register', 'ithemes_repository_name_updater_register' );

	require( __DIR__ . '/lib/updater/load.php' );
}
