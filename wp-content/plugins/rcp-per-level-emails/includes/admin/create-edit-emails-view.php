<?php
/**
 * Add/Edit Level Email
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * @var \RCP_Levels $rcp_levels_db
 */
global $rcp_levels_db;

$membership_level_id = isset( $_GET['level-id'] ) ? absint( $_GET['level-id'] ) : 0;
$membership_level    = $rcp_levels_db->get_level( $membership_level_id );
?>
<div class="wrap">
	<?php if ( empty( $membership_level ) ) : ?>
		<div class="error settings-error">
			<p><?php _e( 'Error: Invalid membership level ID.', 'rcp-per-level-emails' ); ?></p>
		</div>
		<?php
		echo '</div>'; // close .wrap

		return;
	endif; ?>

	<h1>
		<?php printf( __( 'Emails for "%s" Membership', 'rcp-per-level-emails' ), esc_html( $membership_level->name ) ); ?>
		<a href="<?php echo esc_url( admin_url( 'admin.php?page=rcp-member-levels' ) ); ?>" class="add-new-h2"><?php _e( 'Back', 'rcp-per-level-emails' ); ?></a>
	</h1>

	<p><?php _e( 'The following template tags can be used in each message and/or subject:', 'rcp-per-level-emails' ); ?></p>
	<?php
	add_filter( 'rcp_email_template_tags', __NAMESPACE__ . '\remove_irrelevant_template_tags', 999, 2 );
	echo rcp_get_emails_tags_list();
	remove_filter( 'rcp_email_template_tags', __NAMESPACE__ . '\remove_irrelevant_template_tags', 999 );
	?>

	<form id="rcp-membership-level-emails" method="POST">
		<?php
		foreach ( get_email_types() as $type_id => $type_name ) {

			if ( ! email_applies_to_level( $type_id, $membership_level ) ) {
				continue;
			}

			$email   = get_email( $membership_level_id, $type_id );
			$subject = ! empty( $email['subject'] ) ? stripslashes( $email['subject'] ) : '';
			$message = ! empty( $email['message'] ) ? $email['message'] : '';
			?>
			<h2><?php printf( __( '%s Email', 'rcp-per-level-emails' ), esc_html( $type_name ) ); ?></h2>

			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row" valign="top">
						<label for="rcp-level-email-subject-<?php echo esc_attr( $type_id ); ?>"><?php _e( 'Email Subject', 'rcp-per-level-emails' ); ?></label>
					</th>
					<td>
						<input type="text" name="rcp_level_email[<?php echo esc_attr( $type_id ); ?>][subject]" id="rcp-level-email-subject-<?php echo esc_attr( $type_id ); ?>" class="regular-text" value="<?php echo esc_attr( $subject ); ?>"/>

						<p class="description"><?php _e( 'The subject line of the email', 'rcp-per-level-emails' ); ?></p>
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">
						<label for="rcp-level-email-message-<?php echo esc_attr( $type_id ); ?>"><?php _e( 'Email Message', 'rcp-per-level-emails' ); ?></label>
					</th>
					<td>
						<?php wp_editor( wpautop( wp_kses_post( wptexturize( $message ) ) ), 'rcp-level-email-message-' . esc_attr( $type_id ), array( 'textarea_name' => 'rcp_level_email[' . esc_attr( $type_id ) . '][message]' ) ); ?>
						<p class="description"><?php _e( 'The email message.', 'rcp-per-level-emails' ); ?></p>
					</td>
				</tr>
				</tbody>
			</table>
			<?php

		}
		?>

		<p class="submit">
			<input type="hidden" name="rcp-action" value="save_membership_level_emails">
			<input type="hidden" name="level_id" value="<?php echo esc_attr( $membership_level_id ); ?>">
			<?php wp_nonce_field( 'rcp_save_membership_level_emails', 'rcp_save_membership_level_emails_nonce' ); ?>
			<?php submit_button( __( 'Save Emails', 'rcp-per-level-emails' ) ); ?>
		</p>
	</form>
</div>
