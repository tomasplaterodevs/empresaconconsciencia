<?php
/**
 * Settings
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * Add "Configure Emails" link to membership level row actions.
 *
 * This is used in RCP 3.1+.
 *
 * @param array  $actions          Default row actions.
 * @param object $membership_level Membership level object.
 *
 * @since 1.0
 * @return array
 */
function membership_level_actions( $actions, $membership_level ) {

	$actions['per_level_emails'] = '<a href="' . esc_url( add_query_arg( 'level-id', absint( $membership_level->id ), admin_url( 'admin.php?page=rcp-level-emails' ) ) ) . '">' . __( 'Configure Emails', 'rcp-per-level-emails' ) . '</a>';

	return $actions;

}

add_filter( 'rcp_membership_levels_list_table_row_actions', __NAMESPACE__ . '\membership_level_actions', 10, 2 );

/**
 * Add "Configure Emails" link to membership level row actions.
 *
 * This is used pre-RCP 3.1.
 *
 * @param object $level Level object from the database.
 *
 * @since 1.0
 * @return void
 */
function backwards_compat_membership_level_actions( $level ) {

	// Bail if on RCP 3.1+. Then we'll use the `rcp_membership_levels_list_table_row_actions` filter.
	if ( version_compare( RCP_PLUGIN_VERSION, '3.1', '>=' ) ) {
		return;
	}

	echo ' | <a href="' . esc_url( add_query_arg( 'level-id', absint( $level->id ), admin_url( 'admin.php?page=rcp-level-emails' ) ) ) . '">' . __( 'Configure Emails', 'rcp-per-level-emails' ) . '</a>';

}

add_action( 'rcp_membership_level_row_actions', __NAMESPACE__ . '\backwards_compat_membership_level_actions' );

/**
 * Save membership level emails.
 *
 * @since 1.0
 * @return void
 */
function save_level_emails() {

	if ( ! current_user_can( 'rcp_manage_levels' ) ) {
		wp_die( __( 'You do not have permission to configure membership emails', 'rcp-per-level-emails' ), __( 'Error', 'rcp-per-level-emails' ), array( 'response' => 403 ) );
	}

	if ( ! wp_verify_nonce( $_POST['rcp_save_membership_level_emails_nonce'], 'rcp_save_membership_level_emails' ) ) {
		wp_die( __( 'Nonce verification failed', 'rcp-per-level-emails' ), __( 'Error', 'rcp-per-level-emails' ), array( 'response' => 403 ) );
	}

	/**
	 * @var \RCP_Levels $rcp_levels_db
	 */
	global $rcp_levels_db;

	$level_id = absint( $_POST['level_id'] );

	rcp_log( sprintf( 'Per-Level Emails: Saving emails for level #%d.', $level_id ) );

	$sanitized_emails = array();
	$inputted_emails  = $_POST['rcp_level_email'];

	foreach ( $inputted_emails as $email_type => $email ) {
		// Skip if they're both empty.
		if ( empty( $email['subject'] ) && empty( $email['message'] ) ) {
			continue;
		}

		$sanitized_emails[ $email_type ]['subject'] = ! empty( $email['subject'] ) ? sanitize_text_field( $email['subject'] ) : '';
		$sanitized_emails[ $email_type ]['message'] = ! empty( $email['message'] ) ? wp_kses_post( $email['message'] ) : '';
	}

	$rcp_levels_db->update_meta( $level_id, 'per_level_emails', $sanitized_emails );

	$redirect_url = add_query_arg( array(
		'level-id'                 => urlencode( $level_id ),
		'rcp_level_emails_message' => 'level_email_updated'
	), admin_url( 'admin.php?page=rcp-level-emails' ) );

	wp_safe_redirect( esc_url_raw( $redirect_url ) );
	exit;

}

add_action( 'rcp_action_save_membership_level_emails', __NAMESPACE__ . '\save_level_emails' );