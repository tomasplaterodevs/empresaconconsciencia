<?php
/**
 * Admin Notices
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * Display admin notices
 *
 * @since 1.0
 * @return void
 */
function admin_notices() {

	$message = ! empty( $_GET['rcp_level_emails_message'] ) ? urldecode( $_GET['rcp_level_emails_message'] ) : false;
	$class   = 'updated';
	$text    = '';

	if ( empty( $message ) ) {
		return;
	}

	if ( ! current_user_can( 'rcp_manage_levels' ) ) {
		return;
	}

	switch ( $message ) {
		case 'level_email_updated' :
			$text = __( 'Emails updated', 'rcp-per-level-emails' );
			break;
	}

	if ( $message ) {
		echo '<div class="' . esc_attr( $class ) . '"><p>' . $text . '</p></div>';
	}

}

add_action( 'admin_notices', __NAMESPACE__ . '\admin_notices' );