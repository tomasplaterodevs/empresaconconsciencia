<?php
/**
 * Admin Pages
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * Create a new submenu page for level emails.
 *
 * @since 1.0
 * @return void
 */
function settings_menu() {

	add_submenu_page( 'rcp-members', __( 'Emails', 'rcp-per-level-emails' ), __( 'Emails', 'rcp-per-level-emails' ), 'rcp_manage_levels', 'rcp-level-emails', __NAMESPACE__ . '\display_emails_page' );

	// Remove the emails page from the menu.
	add_action( 'admin_head', __NAMESPACE__ . '\hide_emails_page' );

}

add_action( 'admin_menu', __NAMESPACE__ . '\settings_menu' );

/**
 * Hide the emails page from the submenu. We only link to it via Restrict > Subscription Levels.
 *
 * @since 1.0
 * @return void
 */
function hide_emails_page() {
	remove_submenu_page( 'rcp-members', 'rcp-level-emails' );
}

/**
 * Render "Edit Emails" page.
 *
 * @since 1.0
 * @return void
 */
function display_emails_page() {
	require_once RCP_PER_LEVEL_EMAILS_PATH . 'includes/admin/create-edit-emails-view.php';
}