<?php
/**
 * Functions
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

/**
 * Returns all available email types that can be set for each level.
 *
 * @since 1.0
 * @return array
 */
function get_email_types() {

	$types = array(
		'active'               => __( 'Paid Membership Activation', 'rcp-per-level-emails' ),
		'active_admin'         => __( 'Paid Membership Activation (Admin)', 'rcp-per-level-emails' ),
		'free'                 => __( 'Free Membership Activation', 'rcp-per-level-emails' ),
		'free_admin'           => __( 'Free Membership Activation (Admin)', 'rcp-per-level-emails' ),
		'trial'                => __( 'Trial Membership Activation', 'rcp-per-level-emails' ),
		'trial_admin'          => __( 'Trial Membership Activation (Admin)', 'rcp-per-level-emails' ),
		'cancelled'            => __( 'Cancelled Membership', 'rcp-per-level-emails' ),
		'cancelled_admin'      => __( 'Cancelled Membership (Admin)', 'rcp-per-level-emails' ),
		'expired'              => __( 'Expired Membership', 'rcp-per-level-emails' ),
		'expired_admin'        => __( 'Expired Membership (Admin)', 'rcp-per-level-emails' ),
		'payment_received'     => __( 'Payment Received', 'rcp-per-level-emails' ),
		'renewal_failed'       => __( 'Renewal Payment Failed', 'rcp-per-level-emails' ),
		'renewal_failed_admin' => __( 'Renewal Payment Failed (Admin)', 'rcp-per-level-emails' ),
	);

	/*
	 * Add "Payment Received (admin) if on RCP 3.3.9+
	 * We're taking extra care here to insert it after `payment_received` so that they're grouped together.
	 */
	if ( version_compare( RCP_PLUGIN_VERSION, '3.3.9', '>=' ) ) {
		$payment_received_position = array_search( 'payment_received', array_keys( $types ) ) + 1;

		$types = array_merge( array_slice( $types, 0, $payment_received_position ), array(
			'payment_received_admin' => __( 'Payment Received (Admin)', 'rcp-per-level-emails' )
		), array_slice( $types, $payment_received_position ) );
	}

	return $types;

}

/**
 * Get email settings for a given membership level.
 *
 * @param int    $level_id ID of a membership level to get the email for.
 * @param string $type     Type of email to retrieve ("active", "expired", etc.) or blank for all.
 *
 * @since 1.0
 * @return array|false
 */
function get_email( $level_id, $type = '' ) {

	/**
	 * @var \RCP_Levels $rcp_levels_db
	 */
	global $rcp_levels_db;

	$emails = $rcp_levels_db->get_meta( $level_id, 'per_level_emails', true );

	if ( empty( $emails ) || ! is_array( $emails ) ) {
		return false;
	}

	// Return a single type of email.
	if ( ! empty( $type ) && array_key_exists( $type, $emails ) ) {
		return $emails[ $type ];
	}

	// Return all emails.
	return $emails;

}

/**
 * Checks if an email type applies to a certain membership level. For example, if the provided level is free
 * then payment related emails will not apply and this would return false.
 *
 * @param string     $email_type Email type.
 * @param object|int $level      Level object or ID.
 *
 * @since 1.0
 * @return bool
 */
function email_applies_to_level( $email_type, $level ) {

	if ( ! is_object( $level ) ) {
		/**
		 * @var \RCP_Levels $rcp_levels_db
		 */
		global $rcp_levels_db;

		$level = $rcp_levels_db->get_level( $level );
	}

	if ( empty( $level ) ) {
		return false;
	}

	// Figure out if this level is a trial - we'll use this later.
	$is_trial = false;
	if ( ! empty( $level->trial_duration ) || ( empty( $level->price ) && ! empty( $level->duration ) ) ) {
		$is_trial = true;
	}

	// Trial emails do not apply to non-trials.
	if ( in_array( $email_type, array( 'trial', 'trial_admin' ) ) ) {
		if ( ! $is_trial ) {
			return false;
		}
	}

	// "Active" emails do not apply to free levels or trials.
	if ( in_array( $email_type, array( 'active', 'active_admin' ) ) && ( empty( $level->price ) || $is_trial ) ) {
		return false;
	}

	// "Free" emails do not apply to paid levels or trials.
	if ( in_array( $email_type, array( 'free', 'free_admin' ) ) && ( ! empty( $level->price ) || $is_trial ) ) {
		return false;
	}

	// Don't show payment emails for free levels.
	if ( in_array( $email_type, array(
			'payment_received',
			'payment_received_admin',
			'renewal_failed',
			'renewal_failed_admin'
		) ) && empty( $level->price ) ) {
		return false;
	}

	return true;

}