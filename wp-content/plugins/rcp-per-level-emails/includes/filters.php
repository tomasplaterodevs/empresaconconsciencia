<?php
/**
 * Filters
 *
 * @package   rcp-per-level-emails
 * @copyright Copyright (c) 2020, Restrict Content Pro team
 * @license   GPL2+
 */

namespace RCP\Addon\PerLevelEmails;

use RCP_Emails;
use RCP_Membership;

/**
 * Removes irrelevant template tags from the list to prevent them from showing up on the
 * per-level emails UI. This removes tags including: email verification link and Group
 * Accounts tags.
 *
 * @param array      $email_tags Available email tags.
 * @param RCP_Emails $emails     Emails class.
 *
 * @since 1.0
 * @return array
 */
function remove_irrelevant_template_tags( $email_tags, $emails ) {

	foreach ( $email_tags as $key => $tag_details ) {
		if ( 'verificationlink' == $tag_details['tag'] || false !== strpos( $tag_details['tag'], 'group' ) || false !== strpos( $tag_details['tag'], 'invite' ) ) {
			unset( $email_tags[ $key ] );
		}
	}

	return $email_tags;

}

/**
 * Set the email message and subject based on the user's current membership level.
 *
 * @param string         $message    Default email message.
 * @param int            $user_id    ID of the user.
 * @param string         $status     User's status, to determine which email to send.
 * @param RCP_Membership $membership Membership object.
 *
 * @since 1.0
 * @return string
 */
function set_email_contents_for_level( $message, $user_id, $status, $membership ) {

	$level_id       = $membership->get_object_id();
	$email_type     = $status;
	$current_filter = current_filter();

	// Renewal payment failed email type isn't based on status, so we need to check for that.
	if ( strpos( $current_filter, 'renewal_payment_failed' ) !== false ) {
		$email_type = 'renewal_failed';
	}

	// Adjust for admin emails.
	if ( strpos( $current_filter, 'admin' ) !== false ) {
		$email_type .= '_admin';
	}

	rcp_log( sprintf( 'Per-Level Emails: Checking for per-level email for level ID #%d, using filter %s.', $level_id, $current_filter ) );

	$level_email = get_email( $level_id, $email_type );

	// Figure out if we're replacing the subject or the message.
	$content_type = strpos( $current_filter, 'subject' ) !== false ? 'subject' : 'message';

	if ( ! empty( $level_email[ $content_type ] ) ) {
		rcp_log( sprintf( 'Per-Level Emails: Modifying email %s contents for membership level #%d.', $content_type, $level_id ) );

		$message = $level_email[ $content_type ];
	} else {
		rcp_log( sprintf( 'Per-Level-Emails: No custom email %s detected for level #%d - reverting to default.', $content_type, $level_id ) );
	}

	return $message;

}

/**
 * Setup all the normal status filters.
 */
$filters = array(
	'rcp_subscription_active_email',
	'rcp_subscription_active_subject',
	'rcp_email_admin_membership_active_message',
	'rcp_email_admin_membership_active_subject',
	'rcp_subscription_cancelled_email',
	'rcp_subscription_cancelled_subject',
	'rcp_email_admin_membership_cancelled_message',
	'rcp_email_admin_membership_cancelled_subject',
	'rcp_subscription_expired_email',
	'rcp_subscription_expired_subject',
	'rcp_email_admin_membership_expired_message',
	'rcp_email_admin_membership_expired_subject',
	'rcp_subscription_free_email',
	'rcp_subscription_free_subject',
	'rcp_email_admin_message_membership_free_message',
	'rcp_email_admin_subject_membership_free_subject',
	'rcp_subscription_trial_email',
	'rcp_subscription_trial_subject',
	'rcp_email_admin_membership_trial_message',
	'rcp_email_admin_membership_trial_subject',
	'rcp_subscription_renewal_payment_failed_email',
	'rcp_subscription_renewal_payment_failed_subject',
	'rcp_subscription_renewal_payment_failed_admin_email',
	'rcp_subscription_renewal_payment_failed_admin_subject'
);

foreach ( $filters as $filter ) {
	add_filter( $filter, __NAMESPACE__ . '\set_email_contents_for_level', 10, 4 );
}

/**
 * Modify the contents of the "Payment Received" emails. This is in a separate function because the
 * arguments are different.
 *
 * @param string $message    Default email message.
 * @param int    $payment_id ID of the payment that was made.
 * @param array  $payment    Array of payment object data.
 *
 * @since 1.0
 * @return string
 */
function set_payment_received_email_contents( $message, $payment_id, $payment ) {

	$current_filter = current_filter();
	$email_type     = false !== strpos( $current_filter, 'admin' ) ? 'payment_received_admin' : 'payment_received';
	$content_type   = false !== strpos( $current_filter, 'subject' ) ? 'subject' : 'message';
	$level_id       = ! empty( $payment['object_id'] ) ? $payment['object_id'] : false;

	if ( empty( $level_id ) ) {
		rcp_log( sprintf( 'Per-Level Emails: No membership level ID found in filter %s. Exiting.', $current_filter ) );

		return $message;
	}

	rcp_log( sprintf( 'Per-Level Emails: Checking for per-level email for level ID #%d, using filter %s.', $level_id, $current_filter ) );

	$level_email = get_email( $level_id, $email_type );

	if ( ! empty( $level_email[ $content_type ] ) ) {
		rcp_log( sprintf( 'Per-Level Emails: Modifying email %s contents for membership level #%d.', $content_type, $level_id ) );

		$message = $level_email[ $content_type ];
	} else {
		rcp_log( sprintf( 'Per-Level-Emails: No custom email %s detected for level #%d - reverting to default.', $content_type, $level_id ) );
	}

	return $message;

}

add_filter( 'rcp_payment_received_email', __NAMESPACE__ . '\set_payment_received_email_contents', 10, 3 );
add_filter( 'rcp_email_payment_received_subject', __NAMESPACE__ . '\set_payment_received_email_contents', 10, 3 );
add_filter( 'rcp_payment_received_admin_email', __NAMESPACE__ . '\set_payment_received_email_contents', 10, 3 );
add_filter( 'rcp_email_payment_received_admin_subject', __NAMESPACE__ . '\set_payment_received_email_contents', 10, 3 );