<?php
/**
 * Subscription settings
 *
 * @package     RCP\ActiveCampaign\Admin\Subscription\MetaBoxes
 * @since       1.0.0
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Add per-level setting fields
 *
 * @since       1.0.0
 * @return      void
 */
function rcp_activecampaign_add_subscription_settings() {
	?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="rcp-activecampaign-list"><?php _e( 'ActiveCampaign List', 'rcp-activecampaign' ); ?></label>
			</th>
			<td>
				<?php
				$lists = rcp_activecampaign()->api_helper->get_lists();

				$saved_list = ! empty( $_GET['edit_subscription'] ) ? rcp_activecampaign_get_membership_level_list( absint( $_GET['edit_subscription'] ) ) : false;

				if ( ! rcp_activecampaign()->api_helper->activecampaign ) {
					_e( 'Enter valid API details to select a list.', 'rcp-activecampaign' );
				} elseif ( empty( $lists ) ) {
					_e( 'No lists found.', 'rcp-activecampaign' );
				} else {
					?>
					<select name="activecampaign-list" id="rcp-activecampaign-list">
						<option value="inherit"<?php echo selected( $saved_list, 'inherit', false ); ?>><?php _e( 'Use System Default', 'rcp-activecampaign' ); ?></option>
						<?php
						foreach( $lists as $list_id => $list_name ) {
							echo '<option value="' . esc_attr( $list_id ) . '"' . selected( $saved_list, $list_id, false ) . '>' . esc_html( $list_name ) . '</option>';
						}
						?>
					</select>
					<p class="description"><?php _e( 'The ActiveCampaign list to subscribe users to at this level.', 'rcp-activecampaign' ); ?></p>
					<?php
				}
				?>
			</td>
		</tr>
	<?php
}
add_action( 'rcp_add_subscription_form', 'rcp_activecampaign_add_subscription_settings' );
add_action( 'rcp_edit_subscription_form', 'rcp_activecampaign_add_subscription_settings' );


/**
 * Store the ActiveCampaign list in subscription meta
 *
 * @since       1.0.0
 * @param       int $level_id The subscription ID
 * @param       array $args Arguements passed to the action
 */
function rcp_activecampaign_save_subscription( $level_id = 0, $args = array() ) {
	/**
	 * @var RCP_Levels $rcp_levels_db
	 */
	global $rcp_levels_db;

	if ( ! empty( $_POST['activecampaign-list'] ) && 'inherit' !== $_POST['activecampaign-list'] ) {
		$rcp_levels_db->update_meta( $level_id, 'activecampaign_list', sanitize_text_field( $_POST['activecampaign-list'] ) );
	} else {
		$rcp_levels_db->delete_meta( $level_id, 'activecampaign_list' );
	}
}
add_action( 'rcp_add_subscription', 'rcp_activecampaign_save_subscription', 10, 2 );
add_action( 'rcp_edit_subscription_level', 'rcp_activecampaign_save_subscription', 10, 2 );