<?php
/**
 * Register settings
 *
 * @package     RCP\ActiveCampaign\Admin\Settings\Register
 * @since       1.0.0
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Register the ActiveCampaign settings
 *
 * @since       1.0.0
 * @return      void
 */
function rcp_activecampaign_register_settings() {
	register_setting( 'rcp_activecampaign_settings_group', 'rcp_activecampaign_settings', 'rcp_activecampaign_sanitize_settings' );
}
add_action( 'admin_init', 'rcp_activecampaign_register_settings', 100 );


/**
 * Sanitize settings
 *
 * @since       1.0.0
 * @param       array $settings The settings to save
 * @return      array $settings The settings to save
 */
function rcp_activecampaign_sanitize_settings( $settings ) {
	$saved_settings = get_option( 'rcp_activecampaign_settings' );

	// Delete transient on key change
	if( $saved_settings['api_key'] || $saved_settings['api_url'] ) {
		if( ( trim( $saved_settings['api_key'] ) !== trim( $settings['api_key'] ) ) || ( trim( $saved_settings['api_url'] ) !== trim( $settings['api_url'] ) ) ) {
			delete_transient( 'rcp_activecampaign_lists' );
		}
	}

	return $settings;
}


/**
 * Add the ActiveCampaign Pro menu item
 *
 * @since       1.0.0
 * @return      void
 */
function rcp_activecampaign_admin_menu() {
	add_submenu_page(
		'rcp-members',
		__( 'ActiveCampaign Settings', 'rcp-activecampaign' ),
		__( 'ActiveCampaign', 'rcp-activecampaign' ),
		'manage_options',
		'rcp-activecampaign',
		'rcp_activecampaign_render_settings_page'
	);
}
add_action( 'admin_menu', 'rcp_activecampaign_admin_menu', 100 );


/**
 * Render the settings page
 *
 * @since       1.0.0
 * @return      void
 */
function rcp_activecampaign_render_settings_page() {
	$settings   = get_option( 'rcp_activecampaign_settings' );
	$saved_list = isset( $settings['saved_list'] ) ? $settings['saved_list'] : false;
	$saved_tags = isset( $settings['saved_tags'] ) ? $settings['saved_tags'] : array();

	if ( empty( $settings['default_checkbox_state'] ) ) {
		$settings['default_checkbox_state'] = 'checked';
	}

	echo '<div class="wrap">';
	echo '<h2>' . esc_html( get_admin_page_title() ) . '</h2>';

	if( isset( $_REQUEST['updated'] ) && $_REQUEST['updated'] !== false ) {
		echo '<div class="updated fade"><p><strong>' . __( 'Options saved', 'rcp-activecampaign' ) . '</strong></p></div>';
	}
	?>

	<form method="post" action="options.php" class="rcp_options_form">
		<?php settings_fields( 'rcp_activecampaign_settings_group' ); ?>
		<?php $lists = rcp_activecampaign()->api_helper->get_lists(); ?>

		<table class="form-table">
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[api_url]"><?php _e( 'ActiveCampaign API URL', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<input class="regular-text" type="text" id="rcp_activecampaign_settings[api_url]" name="rcp_activecampaign_settings[api_url]" value="<?php echo ( isset( $settings['api_url'] ) ? $settings['api_url'] : '' ); ?>" />
					<div class="description"><?php _e( 'Your ActiveCampaign API URL can be found on the ActiveCampaign Settings &rarr; Developer page.', 'rcp-activecampaign' ); ?></div>
				</td>
			</tr>
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[api_key]"><?php _e( 'ActiveCampaign API Key', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<input class="regular-text" type="text" id="rcp_activecampaign_settings[api_key]" name="rcp_activecampaign_settings[api_key]" value="<?php echo ( isset( $settings['api_key'] ) ? $settings['api_key'] : '' ); ?>" />
					<div class="description"><?php _e( 'Your ActiveCampaign API key can be found on the ActiveCampaign Settings &rarr; Developer page.', 'rcp-activecampaign' ); ?></div>
				</td>
			</tr>
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[saved_list]"><?php _e( 'Default List', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<?php
					if ( ! rcp_activecampaign()->api_helper->activecampaign ) {
						_e( 'Please enter valid API details to choose a default list.', 'rcp-activecampaign' );
					} elseif ( empty( $lists ) ) {
						_e( 'No lists found.', 'rcp-activecampaign' );
					} else {
						?>
						<select id="rcp_activecampaign_settings[saved_list]" name="rcp_activecampaign_settings[saved_list]">
							<?php
							foreach( $lists as $list_id => $list_name ) {
								echo '<option value="' . esc_attr( $list_id ) . '"' . selected( $saved_list, $list_id, false ) . '>' . esc_html( $list_name ) . '</option>';
							} ?>
						</select>
						<div class="description"><?php _e( 'Choose the list to subscribe users to if no per-level list is selected.', 'rcp-activecampaign' ); ?></div>
						<?php
					}
					?>
				</td>
			</tr>
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[signup_label]"><?php _e( 'List Label', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<input class="regular-text" type="text" id="rcp_activecampaign_settings[signup_label]" name="rcp_activecampaign_settings[signup_label]" value="<?php echo ( isset( $settings['signup_label'] ) && ! empty( $settings['signup_label'] ) ? $settings['signup_label'] : __( 'Signup for Newsletter', 'rcp-activecampaign' ) ); ?>" />
					<div class="description"><?php _e( 'Enter the label to be used for the "Signup for Newsletter" checkbox.', 'rcp-activecampaign' ); ?></div>
				</td>
			</tr>
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[auto_subscribe]"><?php _e( 'Auto Subscribe', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<input type="checkbox" id="rcp_activecampaign_settings[auto_subscribe]" name="rcp_activecampaign_settings[auto_subscribe]" value="1" <?php echo ( isset( $settings['auto_subscribe'] ) ? checked( $settings['auto_subscribe'], 1, false ) : '' ); ?> />
					<span class="description"><?php _e( 'Check to hide the subscribe checkbox and automatically subscribe users.', 'rcp-activecampaign' ); ?></span>
				</td>
			</tr>
			<tr>
				<th>
					<label for="rcp_activecampaign_settings[default_checkbox_state]"><?php _e( 'Opt-In Checkbox Default State', 'rcp-activecampaign' ); ?></label>
				</th>
				<td>
					<select id="rcp_activecampaign_settings[default_checkbox_state]" name="rcp_activecampaign_settings[default_checkbox_state]">
						<option value="checked" <?php selected( 'checked' == $settings['default_checkbox_state'] ); ?>><?php _e( 'Checked', 'rcp-activecampaign' ); ?></option>
						<option value="unchecked" <?php selected( $settings['default_checkbox_state'], 'unchecked' ); ?>><?php _e( 'Unchecked', 'rcp-activecampaign' ); ?></option>
					</select>
					<div class="description"><?php _e( 'Choose whether you want the opt-in on the registration form checked or unchecked by default.', 'rcp-activecampaign' ); ?></div>
				</td>
			</tr>
		</table>

		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'rcp-activecampaign' ); ?>" />
		</p>
	</form>
	<?php
	echo '</div>';
}