<?php
/**
 * Subscription settings
 *
 * @package     RCP\ActiveCampaign\Template
 * @since       1.0.0
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Add the subscription field to the registration form
 *
 * @since       1.0.0
 * @return      void
 */
function rcp_activecampaign_add_fields() {
	ob_start();
	if( rcp_activecampaign_show_checkbox() ) {
		$settings = get_option( 'rcp_activecampaign_settings' );
		$checked  = empty( $settings['default_checkbox_state'] ) || 'checked' == $settings['default_checkbox_state'];

		if( isset( $settings['auto_subscribe'] ) ) {
			echo '<input id="rcp_activecampaign_signup" name="rcp_activecampaign_signup" type="hidden" value="true" />';
		} else {
			echo '<p>';
			echo '<input id="rcp_activecampaign_signup" name="rcp_activecampaign_signup" type="checkbox" ' . checked( $checked, true, false ) . ' />';
			echo '<label for="rcp_activecampaign_signup">' . ( isset( $settings['signup_label'] ) && ! empty( $settings['signup_label'] ) ? $settings['signup_label'] : __( 'Signup for Newsletter', 'rcp-activecampaign' ) ) . '</label>';
			echo '</p>';
		}
	}
	echo ob_get_clean();
}
add_action( 'rcp_before_registration_submit_field', 'rcp_activecampaign_add_fields', 100 );