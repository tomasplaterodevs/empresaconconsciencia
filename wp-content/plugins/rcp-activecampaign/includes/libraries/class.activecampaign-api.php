<?php
/**
 * ActiveCampaign API Handler
 *
 * @package     RCP\ActiveCampaign\API
 * @since       1.0.0
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * ActiveCampaign API handler class
 *
 * @since       1.0.0
 */
class RCP_ActiveCampaign_API {


	/**
	 * @var         object $activecampaign The ActiveCampaign API object
	 * @since       1.0.0
	 */
	public $activecampaign;


	/**
	 * @var         string $api_key The ActiveCampaign API key
	 * @since       1.0.0
	 */
	public $api_key;


	/**
	 * @var         string $api_url The ActiveCampaign API URL
	 * @since       1.0.0
	 */
	public $api_url;


	/**
	 * @var         array $settings ActiveCampaign Pro settings
	 * @since       1.0.0
	 */
	public $settings;


	/**
	 * @var         array $lists Available lists
	 * @since       1.0.0
	 */
	public $lists;


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function __construct() {
		if ( ! $this->activecampaign ) {
			$this->setup_api();
		}

		// Maybe set a flag to sign up the user
		add_action( 'rcp_form_processing', array( $this, 'maybe_signup' ), 10, 8 );

		if ( function_exists( 'rcp_get_membership' ) ) {
			// RCP 3.0+ - maybe add to list
			add_action( 'rcp_successful_registration', array( $this, 'maybe_subscribe_to_list' ), 10, 3 );
		} else {
			// RCP 2.x and below - maybe add to list.
			add_action( 'rcp_set_status', array( $this, 'maybe_add_to_list' ), 10, 4 );
		}

		// Update ActiveCampaign when user changes email
		add_action( 'profile_update', array( $this, 'update_subscription' ), 10, 2 );
		add_action( 'rcp_user_profile_updated', array( $this, 'update_subscription_from_frontend' ), 10, 2 );
	}


	/**
	 * Setup the API object
	 *
	 * @access      private
	 * @since       1.0.0
	 * @return      void
	 */
	private function setup_api() {
		if( ! $this->api_key || ! $this->api_url ) {
			$this->settings = get_option( 'rcp_activecampaign_settings' );

			if( ! empty( $this->settings['api_key'] ) && ! empty( $this->settings['api_url'] ) ) {
				$this->api_key = $this->settings['api_key'];
				$this->api_url = $this->settings['api_url'];

				if( ! class_exists( 'ActiveCampaign' ) ) {
					$ac_classes = array( 'Connector', 'ActiveCampaign', 'Account', 'Auth', 'Automation', 'Campaign', 'Contact', 'Deal', 'Design', 'Form', 'Group', 'List', 'Message', 'Settings', 'Subscriber', 'Tracking', 'User', 'Webhook' );

					foreach( $ac_classes as $class ) {
						require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/libraries/activecampaign-api/' . $class . '.class.php';
					}
				}

				$this->activecampaign = new ActiveCampaign( $this->api_url, $this->api_key );
			}
		}
	}


	/**
	 * Retrieve the available lists
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      array
	 */
	public function get_lists() {
		if( $this->activecampaign ) {
			delete_transient( 'rcp_activecampaign_list_data' );
			$lists = get_transient( 'rcp_activecampaign_list_data' );

			if( $lists === false ) {
				$request = $this->activecampaign->api( 'list/paginator?limit=999' );

				if( $request && is_object( $request ) ) {
					if( $request->result_code == 1 ) {
						$lists = $request;

						set_transient( 'rcp_activecampaign_list_data', $request, 24*24*24 );
					}
				}
			}

			if( ! empty( $lists ) && ! empty( $lists->rows ) ) {
				foreach( $lists->rows as $list ) {
					$this->lists[$list->id] = $list->name;
				}
			}
		}

		return (array) $this->lists;
	}


	/**
	 * Retrieve the list to signup for
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       int $level_id The ID of the subscription level to lookup
	 * @return      string $list The ActiveCampaign list to signup for
	 */
	public function get_list( $level_id ) {
		$list = rcp_activecampaign_get_membership_level_list( $level_id );

		if ( empty( $list ) || 'inherit' === $list ) {
			$list = $this->settings['saved_list'];
		}

		return apply_filters( 'rcp_activecampaign_get_list', $list, $level_id );
	}


	/**
	 * Subscribe an email to ActiveCampaign
	 *
	 * @access      public
	 * @since       1.0.0
	 *
	 * @param string               $email      The email address to subscribe
	 * @param string               $first_name The first name of the user
	 * @param string               $last_name  The last name of the user
	 * @param RCP_Member           $member     Member object.
	 * @param RCP_Membership|false $membership Membership object.
	 *
	 * @return      array|false Array of data if added successfully, false otherwise
	 */
	public function subscribe( $email, $first_name, $last_name, $member, $membership = false ) {
		if ( class_exists( 'RCP_Membership' ) && $membership instanceof RCP_Membership ) {
			$list = $this->get_list( $membership->get_object_id() );
		} else {
			$list = $this->get_list( $member->get_subscription_id() );
		}

		if ( $list ) {

			rcp_log( sprintf( 'ActiveCampaign - Subscribing user #%d to list %s.', $member->ID, $list ) );

			$data = array(
				'email'                 => $email,
				'p[' . $list . ']'      => $list,
				'status[' . $list . ']' => 1, // Active
				'first_name'            => $first_name,
				'last_name'             => $last_name
			);

			$result = $this->activecampaign->api( 'contact/sync', apply_filters( 'rcp_activecampaign_subscribe_data', $data, $member, $list ) );

			if ( $result->result_code == 1 ) {
				rcp_log( 'ActiveCampaign - Subscribe successful.' );

				return array( 'list' => array( $list ), 'id' => $result->subscriber_id );
			}

		}

		rcp_log( sprintf( 'ActiveCampaign - Failed to subscriber user #%d - missing list ID.', $member->ID ), true );

		return false;
	}


	/**
	 * Maybe sign up a given user
	 *
	 * @access      public
	 * @since       1.0.0
	 *
	 * @param array                $posted              The fields posted by the submission form
	 * @param int                  $user_id             The ID of this user
	 * @param float                $price               Price of the membership.
	 * @param RCP_Customer|false   $customer            Customer object.
	 * @param int                  $membership_id       ID of the membership for this registration.
	 * @param RCP_Membership|false $previous_membership Previous membership, if this is an upgrade/downgrade.
	 * @param string               $registration_type   Type of registration (new, upgrade, downgrade, renewal).
	 *
	 * @return      void
	 */
	public function maybe_signup( $posted, $user_id, $price = 0.00, $payment_id = 0, $customer = false, $membership_id = 0, $previous_membership = false, $registration_type = 'new' ) {

		if ( ! $this->activecampaign ) {
			rcp_log( sprintf( 'ActiveCampaign - Not adding user #%d to list - missing API information.', $user_id ), true );

			return;
		}

		if ( empty( $posted['rcp_activecampaign_signup'] ) ) {
			rcp_log( sprintf( 'ActiveCampaign - Not adding user #%d to list - opt-in not selected.', $user_id ) );

			delete_user_meta( $user_id, 'rcp_pending_activecampaign_signup' );

			if ( function_exists( 'rcp_delete_membership_meta' ) && ! empty( $membership_id ) ) {
				rcp_delete_membership_meta( $membership_id, 'activecampaign_pending_signup' );
			}

			return;
		}

		rcp_log( sprintf( 'ActiveCampaign - Setting sign up flag for user #%d.', $user_id ) );

		if ( function_exists( 'rcp_add_membership_meta' ) && ! empty( $membership_id ) ) {
			rcp_add_membership_meta( $membership_id, 'activecampaign_pending_signup', date( 'Y-m-d H:i:s' ) );
		} else {
			update_user_meta( $user_id, 'rcp_pending_activecampaign_signup', true );
		}
	}

	/**
	 * Maybe add member to the ActiveCampaign list when their registration completes.
	 *
	 * @param RCP_Member     $member
	 * @param RCP_Customer   $customer
	 * @param RCP_Membership $membership
	 *
	 * @since 1.1
	 * @return void
	 */
	public function maybe_subscribe_to_list( $member, $customer, $membership ) {

		if ( ! rcp_get_membership_meta( $membership->get_id(), 'activecampaign_pending_signup', true ) ) {
			return;
		}

		rcp_log( sprintf( 'ActiveCampaign - Beginning subscribe process for membership #%d.', $membership->get_id() ) );

		if ( ! $member instanceof RCP_Member ) {
			return;
		}

		$subscribed = $this->subscribe( $member->user_email, $member->first_name, $member->last_name, $member, $membership );

		if ( $subscribed ) {
			// Merge new list ID with existing list ID(s).
			$existing_subscriber_data = get_user_meta( $customer->get_user_id(), 'rcp_subscribed_to_activecampaign', true );
			if ( is_array( $existing_subscriber_data ) && ! empty( $existing_subscriber_data['list'] ) ) {
				$list_data          = is_array( $existing_subscriber_data['list'] ) ? $existing_subscriber_data['list'] : array( $existing_subscriber_data['list'] );
				$subscribed['list'] = array_merge( $subscribed['list'], $list_data );
			}

			update_user_meta( $customer->get_user_id(), 'rcp_subscribed_to_activecampaign', $subscribed );
			delete_user_meta( $customer->get_user_id(), 'rcp_pending_activecampaign_signup' );
			rcp_delete_membership_meta( $membership->get_id(), 'activecampaign_pending_signup' );
		}

	}

	/**
	 * Add member to the ActiveCampaign list when their account is activated.
	 *
	 * @deprecated 1.1 In favour of `maybe_subscribe_to_list()`. This is just used for RCP 2.x backwards compat.
	 * @see        maybe_subscribe_to_list()
	 *
	 * @param string     $status     New status.
	 * @param int        $user_id    ID of the user.
	 * @param string     $old_status Previous status.
	 * @param RCP_Member $member     Member object.
	 *
	 * @since      1.0.2
	 * @return void
	 */
	public function maybe_add_to_list( $status, $user_id, $old_status, $member ) {

		if ( ! in_array( $status, array( 'active', 'free' ) ) ) {
			return;
		}

		if ( ! get_user_meta( $user_id, 'rcp_pending_activecampaign_signup', true ) ) {
			return;
		}

		$subscribed = $this->subscribe( $member->user_email, $member->first_name, $member->last_name, $member );

		if ( $subscribed ) {
			update_user_meta( $user_id, 'rcp_subscribed_to_activecampaign', $subscribed );
			delete_user_meta( $user_id, 'rcp_pending_activecampaign_signup' );
		}

	}


	/**
	 * Update subscription when user changes their email
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       int $user_id The ID of the user
	 * @param       object $old_user_data The old data for the user
	 * @return      void
	 */
	public function update_subscription( $user_id, $old_user_data ) {
		if( $this->activecampaign ) {
			$user_data = get_userdata( $user_id );

			$new_email = $user_data->user_email;
			$old_email = $old_user_data->user_email;

			if( $new_email != $old_email ) {
				$subscriber = get_user_meta( $user_id, 'rcp_subscribed_to_activecampaign', true );

				if ( is_array( $subscriber ) && ! empty( $subscriber['id'] ) && ! empty( $subscriber['list'] ) ) {
					$data = array(
						'id'        => $subscriber['id'],
						'email'     => $new_email,
						'overwrite' => 0
					);

					$lists = is_array( $subscriber['list'] ) ? $subscriber['list'] : array( $subscriber['list'] );

					foreach ( $lists as $list_id ) {
						$data[ 'p[' . $subscriber['list'] . ']' ] = $list_id;
					}

					$result = $this->activecampaign->api( 'contact/edit', $data );
				}
			}
		}
	}


	/**
	 * Update subscription when user changes their email from frontend form
	 *
	 * @access      public
	 * @since       1.0.
	 * @param       int $user_id The ID of the user
	 * @param       object $user_data The data for the user
	 * @return      void
	 */
	public function update_subscription_from_frontend( $user_id, $user_data ) {
		$settings = get_option( 'rcp_activecampaign_settings' );

		if( $this->activecampaign ) {
			$email      = $user_data['user_email'];
			$subscriber = get_user_meta( $user_id, 'rcp_subscribed_to_activecampaign', true );

			if ( is_array( $subscriber ) && ! empty( $subscriber['id'] ) && ! empty( $subscriber['list'] ) ) {
				$data = array(
					'id'        => $subscriber['id'],
					'email'     => $email,
					'overwrite' => 0
				);

				$lists = is_array( $subscriber['list'] ) ? $subscriber['list'] : array( $subscriber['list'] );

				foreach ( $lists as $list_id ) {
					$data[ 'p[' . $subscriber['list'] . ']' ] = $list_id;
				}

				$result = $this->activecampaign->api( 'contact/edit', $data );
			}
		}
	}
}
