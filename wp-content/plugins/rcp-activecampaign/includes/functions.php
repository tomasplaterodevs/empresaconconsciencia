<?php
/**
 * Helper functions
 *
 * @package     RCP\ActiveCampaign\Functions
 * @since       1.0.0
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Check if we should show the subscription field
 *
 * @since       1.0.0
 * @param       int $level A specific level to check
 * @return      bool $show True to show, false otherwise
 */
function rcp_activecampaign_show_checkbox() {
	$settings = get_option( 'rcp_activecampaign_settings' );
	$lists    = get_option( 'rcp_activecampaign_subscription_lists' );
	$show     = false;

	if( ! empty( $settings['api_key'] ) ) {
		if( ! empty( $settings['saved_list'] ) ) {
			$show = true;
		}

		if( is_array( $lists ) ) {
			$show = true;
		}
	}

	return $show;
}

/**
 * Get the list specifically assigned to an individual membership level. If no list is specified then
 * will return `false` and the global list should be used instead.
 *
 * @param int $level_id ID of the membership level to get the list of.
 *
 * @since 1.1
 * @return string|false
 */
function rcp_activecampaign_get_membership_level_list( $level_id ) {

	/**
	 * @var RCP_Levels $rcp_levels_db
	 */
	global $rcp_levels_db;

	// Check new meta first.
	$list_id = $rcp_levels_db->get_meta( $level_id, 'activecampaign_list', true );

	if ( ! empty( $list_id ) ) {
		return $list_id;
	}

	// If we don't have anything in meta, check the old option key to see if we need to migrate that over.
	$level_lists = get_option( 'rcp_activecampaign_subscription_lists' );

	if ( ! empty( $level_lists[ $level_id ] ) && 'inherit' !== $level_lists[ $level_id ] ) {
		// Update meta so we have it for next time.
		$rcp_levels_db->update_meta( $level_id, 'activecampaign_list', sanitize_text_field( $level_lists[ $level_id ] ) );

		return $level_lists[ $level_id ];
	}

	return false;

}