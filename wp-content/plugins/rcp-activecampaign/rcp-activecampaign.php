<?php
/**
 * Plugin Name:     Restrict Content Pro - ActiveCampaign
 * Plugin URI:      https://restrictcontentpro.com/downloads/activecampaign/
 * Description:     Include an ActiveCampaign signup option with your RCP registration form
 * Version:         1.1.1
 * Author:          Sandhills Development, LLC
 * Author URI:      https://sandhillsdev.com
 * Text Domain:     rcp-activecampaign
 * iThemes Package: rcp-activecampaign
 *
 * @package         RCP\ActiveCampaign
 * @author          Daniel J Griffiths <dgriffiths@section214.com>
 * @copyright       Copyright (c) 2015, Daniel J Griffiths
 */


// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}


if( ! class_exists( 'RCP_ActiveCampaign' ) ) {


	/**
	 * Main RCP_ActiveCampaign class
	 *
	 * @since       1.0.0
	 */
	class RCP_ActiveCampaign {


		/**
		 * @var         RCP_ActiveCampaign $instance The one true RCP_ActiveCampaign
		 * @since       1.0.0
		 */
		private static $instance;


		/**
		 * @var         RCP_ActiveCampaign_API $api_helper The ActiveCampaign API helper object
		 * @since       1.0.0
		 */
		public $api_helper;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      object self::$instance The one true RCP_ActiveCampaign
		 */
		public static function instance() {
			if( ! self::$instance ) {
				self::$instance = new RCP_ActiveCampaign();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->load_textdomain();
				self::$instance->hooks();
				self::$instance->api_helper = new RCP_ActiveCampaign_API();
			}

			return self::$instance;
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		public function setup_constants() {
			// Plugin version
			define( 'RCP_ACTIVECAMPAIGN_VER', '1.1.1' );

			// Plugin path
			define( 'RCP_ACTIVECAMPAIGN_DIR', plugin_dir_path( __FILE__ ) );

			// Plugin URL
			define( 'RCP_ACTIVECAMPAIGN_URL', plugin_dir_url( __FILE__ ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/functions.php';
			require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/template.php';
			require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/libraries/class.activecampaign-api.php';

			if( is_admin() ) {
				require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/admin/settings/register.php';
				require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/admin/subscription/meta-boxes.php';
				require_once RCP_ACTIVECAMPAIGN_DIR . 'includes/admin/user/profile.php';
			}
		}


		/**
		 * Run action and filter hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {

			if( class_exists( 'RCP_Add_On_Updater' ) ) {
				$updater = new RCP_Add_On_Updater( 215, __FILE__, RCP_ACTIVECAMPAIGN_VER );
			}

		}


		/**
		 * Internationalization
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public static function load_textdomain() {
			// Set filter for language directory
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'rcp_activecampaign_languages_directory', $lang_dir );

			// Traditional WordPress plugin locale filter
			$locale = apply_filters( 'plugin_locale', get_locale(), 'rcp-activecampaign' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'rcp-activecampaign', $locale );

			// Setup paths to current locale file
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/rcp-activecampaign/' . $mofile;

			if( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/rcp-activecampaign/ folder
				load_textdomain( 'rcp-activecampaign', $mofile_global );
			} elseif( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/rcp-activecampaign/languages/ folder
				load_textdomain( 'rcp-activecampaign', $mofile_local );
			} else {
				// Load the default language files
				load_plugin_textdomain( 'rcp-activecampaign', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true RCP_ActiveCampaign
 * instance to functions everywhere
 *
 * @since       1.0.0
 * @return      RCP_ActiveCampaign The one true RCP_ActiveCampaign
 */
function rcp_activecampaign() {
	return RCP_ActiveCampaign::instance();
}
add_action( 'plugins_loaded', 'rcp_activecampaign' );


if ( ! function_exists( 'ithemes_repository_name_updater_register' ) ) {
	function ithemes_repository_name_updater_register( $updater ) {
		$updater->register( 'rcp-activecampaign', __FILE__ );
	}
	add_action( 'ithemes_updater_register', 'ithemes_repository_name_updater_register' );

	require( __DIR__ . '/lib/updater/load.php' );
}
